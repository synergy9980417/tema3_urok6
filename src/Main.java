import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.net.URL;
import java.net.URLConnection;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) throws IOException {
//        //TIP Press <shortcut actionId="ShowIntentionActions"/> with your caret at the highlighted text
//        // to see how IntelliJ IDEA suggests fixing it.
//        System.out.printf("Hello and welcome!");
//
//        for (int i = 1; i <= 5; i++) {
//            //TIP Press <shortcut actionId="Debug"/> to start debugging your code. We have set one <icon src="AllIcons.Debugger.Db_set_breakpoint"/> breakpoint
//            // for you, but you can always add more by pressing <shortcut actionId="ToggleLineBreakpoint"/>.
//            System.out.println("i = " + i);
//        }


//        Урок 6. Операторы Continue, break.
//        Цель задания:
//        Знакомство с операторами сontinue, break. Совершенствование навыков работы с
//        циклами
//        Задание:
//        1.
//        Пользователь вводит 10 слов в массив. Найдите первое слово, в котором есть две
//        гласные буквы подряд



          System.out.println("!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!");
          String [] stra1 = new String[10];
          System.out.println("Введите 10 строк:");
          Scanner scanner = new Scanner(System.in);
          for (int i=0;i<10;i++){
              System.out.println("вводим слово номер "+ (i+1) +" :");
              stra1[i]=scanner.nextLine();
          }

        for (String str : stra1) {
            if (dveGlasnInStr(str)) break;
        }

//        2.
//        Пользователь вводит массив чисел. Найдите первое четное число

        System.out.println("!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!");
        System.out.println("введите массив чисел");
        int [] intarr2 = new int[108];
        Scanner scanner2 = new Scanner(System.in);
        int i2=0;
        do {
            System.out.println("введите число");
            intarr2[i2] = scanner2.nextInt();
            i2++;
        } while (intarr2[i2-1]%2!=0);
        System.out.println("вот число которое чётное "+intarr2[i2]);

//        3.
//        Найдите первое чётное число в массиве, которое больше 100

        System.out.println("!!!!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!");
        int [] intarr3 = {0,1,2,3,4,100,200,33,300,500};
        for (int i=0;i<intarr3.length;i++){
            if (intarr3[i]>100){
                System.out.println("Элемент "+i+" в массива intarr3 больше 100");
                break;
            }
        }

//        4.
//        Найдите последнее слово в массиве, которое написано ЗаБоРчИкОм (что б
//        узнать, заглавная ли буква, используйте Character.isUpperCase(lett
//                er) )

        System.out.println("!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!");

        String [] strar4 = {"asfsd","AsDfGsO","sdfasfdf","AsDfGsO","sdfasfdf","AsDfGsOO","sdfasfdf"};
        boolean flag4=false;
        for (int i=strar4.length-1;i>0;i--){
            for (int j = 0; j<strar4[i].length(); j++){
               if (j%2==0)
               {
                   if (Character.isUpperCase(strar4[i].charAt(j))) flag4=true;
                   else flag4=false;
               }
               else
               {
                   if (!(flag4&&Character.isLowerCase(strar4[i].charAt(j)))) flag4=false;
               }
            }
            if (flag4){
                System.out.println("это последнее в массиве слово, индекс "+i+" <"+strar4[i]+"> написано строго заборчиком");
                break;
            }
        }

//        5.
//        Выводите числа от 1 до того момента, как сумма всех цифр в числе не будет
//        равна 20 (пример такого числа
//        -
//                875)

        System.out.println("!!!!!!!!!!!!!!!!!!! 5 !!!!!!!!!!!!!!!!");
        String str5=new String();
        int num5;
        for (int i=0;i<1000;i++){
            str5=Integer.toString(i);
            System.out.println(str5);
            num5=0;
            for (int j=0;j<str5.length();j++){
                num5 += Character.getNumericValue(str5.charAt(j));
            }
            if (num5==20){
                System.out.println("вот число сумма цифр которого равна 20 ("+i+")");
                break;
            }
        }


//        6.
//        Выведите все даты невисокосного года. В январе 31 день, феврале
//                -
//                28, далее
//        чередуется
//                -
//                в марте 31, в апреле 30...

        System.out.println("!!!!!!!!!!!!!!!!!!! 6 !!!!!!!!!!!!!!!!");
        byte days=0;
        for(int month=1;month<=12;month++){
            if (month%2==0) {
                days=30;
                if (month==2) days=28;
            }
            else days=31;

            for (int i=1;i<=days;i++){

                System.out.print("день/месяц/год: ("+ i+"/");
                System.out.print(month+"/");
                System.out.println("2024)");
            }
        }

//        7.
//        Сохраняйте снимки NASA с
//        1 января того момента, пока в поле
//        Explanation
//                нет
//        слова “Earth”.

        System.out.println("!!!!!!!!!!!!!!!!!!! 7 !!!!!!!!!!!!!!!!");

        String myIpKey="b8BKYPTRSYvG6dKnwUbPAq8jht3kMxyrJ6WVBVRp";

        byte day=1;
// по одной ссылке через цикл, раз задания про циклы, но так дольше...
        String dd="";
        while (day!=31) {
            if (day<10) dd="0"+day;
            else dd=""+day;

//            System.out.println("!!!!!!!!");
            String page = downloadWebPage("https://api.nasa.gov/planetary/apod?api_key=" + myIpKey + "&start_date=" + "2024-01-"+dd + "&end_date=" + "2024-01-"+dd);
//            System.out.println(page);
            day+=1;

            StringBuilder strb7 =new StringBuilder(page);

            String startStrUrl7="hdurl\":\"";
            String endStrUrl7="\",\"";
            String subUrlstr7=new String();
            int startUrl7;
            int endUrl7;
            try {
                //тут бывает что не изображение а видео поэтому другой формат парсинга. но если неудача пропускаем итерацию
               startUrl7 = strb7.indexOf(startStrUrl7) + startStrUrl7.length();
               endUrl7 = strb7.indexOf(endStrUrl7, startUrl7);
           } catch (Exception e){
               System.out.println(e);
               continue;
           };
            //сюда попадаем если не попадаем в catch значит у нас есть ссылка
            subUrlstr7=strb7.substring(startUrl7, endUrl7);
//            System.out.println(subUrlstr7);
            String startStr7="explanation\":\"";
            int start7=strb7.indexOf(startStr7)+startStr7.length();
            String endStr7="\",\"";
            int end7=strb7.indexOf(endStr7,start7);
//            System.out.println(strb7.substring(start7,end7));
//            System.out.println("!!!!!!!!");
            String strEarth="Earth";
            String subStr7=strb7.substring(start7,end7);
            if (subStr7.indexOf(strEarth)>0){
                System.out.println(subStr7);
                break;
            }
            //если не вышли из цикла сохраняем файл изображения
                //получаем имя первого файла из ссылки
                StringBuilder file=new StringBuilder();
                String[] arrFn=subUrlstr7.split("/");
                file.delete(0,file.length());
                file.append(arrFn[arrFn.length-1]);


                downloadImage(subUrlstr7,"files/"+file.toString());


        }




//        Критерии оценивания:
//        1 балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                написан скелет кода для ввода данных со стороны пользователя
//        3 балла
//                -
//                выполнено более 60% заданий, имеется не более 5 крит
//                ичных
//        замечаний
//        4 балла
//                -
//                выполнено корректно более 80% технических заданий
//        5 баллов
//                -
//                все технические задания выполнены корректно, в полном объеме
//        Задание считается выполненным при условии, что слушатель получил оценку не
//        менее 3 баллов



    }

    static boolean dveGlasnInStr(String str) {
           for (int i=0;i<str.length()-1;i++)
           if (glasn(str.charAt(i))&&glasn(str.charAt(i+1))){
               System.out.println("слово в котором две гласных подряд: "+str);
               return true;}

           return false;
           }


    //метод нахождения гласной для первого задания
    static boolean glasn(char ch){
        char[] chra1 = {'й', 'у', 'е', 'н', 'ъ', 'ы', 'а', 'о', 'э', 'я', 'и', 'ь', 'ю'};
        for (int j = 0; j < chra1.length; j++) 
        if (ch == chra1[j])
            return true;
        return false;
    }

    private static String downloadWebPage(String url) throws IOException{
        StringBuilder result = new StringBuilder();
        String line;
        URLConnection urlConnection = new URL(url).openConnection();
        try (InputStream is = urlConnection.getInputStream();
             BufferedReader br= new BufferedReader(new InputStreamReader(is))){
            while ((line=br.readLine())!=null){
                result.append(line);
            }
        }
        return result.toString();
    }
//Метод скачивания изображения по url
     static void downloadImage(String url, String fullname) throws IOException{
        try(InputStream in = new URL(url).openStream()){
            Files.copy(in, Paths.get(fullname));
        } catch (Exception e) {System.out.println(e);}
    }
}